import request from "./request";

export interface PitScoutData {
    weight?: number,
    width?: number,
    length?: number,
    drive_train_motor_count?: number,
    drive_train_motor_type?: "Mini CIM" | "CIM" | "Neo" | "Falcon",
    drive_train_type?: "Tank" | "Swerve" | "Mecanum" | "Tank Treads",
    wheel_type?: "Traction" | "Colson" | "Pneumatic" | "Omni",
    wheel_diameter?: 4 | 6 | 8,
    programming_language?: "Java" | "C++" | "LabView",
    camera?: boolean,
    notes?: string,
    game_specific: PitScoutGameSpecificData
}

export interface PitScoutGameSpecificData {
    climbing_method?: string,
    move_on_bar?: boolean,
    carry_robot?: boolean,
    low_goal?: boolean,
    outer_goal?: boolean,
    inner_goal?: boolean
}

export class PitScout {
    static async list() {
        return await request.get(`/scouting/pit`);
    }

    static async team(key: string) {
        return await request.get(`/scouting/pit/${key}`);
    }

    static async modify(key: string, data: PitScoutData) {
        return await request.put(`/scouting/pit/${key}`, data);
    }
}
