import request from "./request";

export enum EventType {
    ShotTaken,
    PowerCellCollected,
    ControlPanelStage2,
    ControlPanelStage3,
    ShotBlocked,
    Malfunction,
    PushMatch,
    Climbing
}

export enum ScoreLocation {
    LowGoal,
    OuterGoal,
    InnerGoal
}

export enum CollectionLocation {
    FriendlyLoadingStation,
    OpponentLoadingStation,
    FriendlyTrench,
    OpponentTrench,
    FriendlyBoundaries,
    OpponentBoundaries,
    FriendlySector,
    OpponentSector,
    Other
}

export enum MalfunctionType {
    Break,
    Brownout,
    Disabled,
    Other
}

export interface MatchEvent {
    type: EventType,

    // For ShotTaken
    shots_made?: number,
    shots_taken?: number,
    shot_score_location?: ScoreLocation,
    shot_from?: number

    // For PowerCellCollected
    collected_success?: boolean,
    collected_from?: CollectionLocation,

    // For ControlPanelStage2 & ControlPanelStage3
    cp_start?: number,
    cp_end?: number,
    cp_attempts?: number,
    cp_completed?: boolean,

    // For ShotBlocked
    block_successful?: number,
    block_attempted?: number,

    // For Malfunction
    malfunction_start?: number,
    malfunction_end?: number,
    malfunction_type?: MalfunctionType,

    // For PushMatch
    push_start?: number,
    push_end?: number,
    push_against?: number,
    push_won?: boolean,

    // For Climbing
    climb_start?: number,
    climb_end?: number,
    climb_attempts?: number,
    climb_successful?: boolean,
}

export class MatchScout {
    static async matches(key: string) {
        return await request.get(`/scouting/match/${key}`);
    }

    static async scout(key: string, match: string, team: string, events: MatchEvent[]) {
        return await request.post(`/scouting/match/${key}`, {
            match: match,
            team: team,
            events: events
        });
    }

    static async statistics(key: string) {
        return await request.get(`/scouting/match/${key}/stats`)
    }
}
