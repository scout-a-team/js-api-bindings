import sha256 from 'crypto-js/sha256';
import Hex from 'crypto-js/enc-hex';

import request from './request';

export type Role = "owner" | "administrator" | "mentor" | "member" | "";

export class Users {
    static async list(team: string) {
        return await request.get(`/teams/${team}/users`);
    }

    static async create(team: string, first_name: string, last_name: string, email: string, password: string, role: string) {
        let hashed = Hex.stringify(sha256(password));
        return await request.post(`/teams/${team}/users`, {
            first_name: first_name,
            last_name: last_name,
            email: email,
            password: hashed,
            role: role
        });
    }

    static async describe(team: string, id: string) {
        return await request.get(`/teams/${team}/users/${id}`);
    }

    static async update(team: string, id: string, data: { first_name?: string, last_name?: string, email?: string, role?: string, enabled?: boolean }) {
        return await request.put(`/teams/${team}/users/${id}`, data);
    }

    static async delete(team: string, id: string) {
        return await request.delete(`/teams/${team}/users/${id}`);
    }

    static async enable_2fa(team: string, id: string, totp: boolean=false, webauthn: boolean=false) {
        return await request.put(`/teams/${team}/users/${id}/2fa`, {
            totp: totp,
            webauthn: webauthn
        });
    }

    static async disable_2fa(team: string, id: string, method: string) {
        return await request.delete(`/teams/${team}/users/${id}/2fa`, {
            params: {
                method: method
            }
        });
    }

    static async assign(team: string, id: string, role: Role) {
        return await request.put(`/teams/${team}/users/${id}/assign`, {
            role: role
        });
    }
}
