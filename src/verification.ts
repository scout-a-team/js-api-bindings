import sha256 from 'crypto-js/sha256';
import Hex from 'crypto-js/enc-hex';

import request from "./request";

export enum TeamVerificationMethod {
    WEB_PAGE = 1,
    DNS,
    PHOTO
}

export class Verification {
    static async email_verification(token: string) {
        return await request.get("/verification/email", {
            headers: {
                Authorization: token
            }
        });
    }

    static async get_team_verification() {
        return await request.get("/verification/team");
    }

    static async verify_team(type: TeamVerificationMethod, page?: string, domain?: string, image?: string) {
        return await request.post("/verification/team", {
            type: type,
            page: page,
            domain: domain,
            image: image
        });
    }

    static async reset_password(password: string) {
        let hashed = Hex.stringify(sha256(password));
        return await request.post("/verification/reset-password", {
            password: hashed
        });
    }
}
