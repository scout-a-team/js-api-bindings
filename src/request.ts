import axios, {AxiosResponse} from 'axios';

const url = "http://localhost:8080";

export default axios.create({
    baseURL: url,
    responseType: "json",
    headers: {
        Accept: "application/json"
    }
});

interface Response {
    status: number;
    success: boolean;
    reason?: string;
    data?: any;
}

const request = axios.create({
    baseURL: url,
    responseType: "json",
    headers: {
        Accept: "application/json"
    },
    validateStatus: _ => true
});

export class Requester {
    static async get(path: string, params?: Object) {
        try {
            let response = await request.get(path, { params });

            let returnValue = {
                status: response.status,
                success: response.data.status === "success",
                reason: undefined,
                data: undefined
            };

            if (response.data.reason) returnValue.reason = response.data.reason;
            else returnValue.data = response.data;

            return returnValue
        } catch (e) {
            return {
                status: 500,
                success: false,
                reason: e.message
            }
        }
    }

    static async post(path: string, body?: Object) {

    }

    static async put(path: string, body?: Object) {

    }

    static async delete(path: string, params?: Object) {

    }
}
