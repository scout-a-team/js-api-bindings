import sha256 from 'crypto-js/sha256';
import Hex from 'crypto-js/enc-hex';

import request from './request';

export class Authentication {
    static async register(first_name: string, last_name: string, email: string, password: string, team_number: number) {
        let hashed = Hex.stringify(sha256(password));
        return await request.post("/authentication/register", {
            email: email,
            password: hashed,
            first_name: first_name,
            last_name: last_name,
            team_name: team_number
        });
    }

    static async login(email: string, password: string) {
        let hashed = Hex.stringify(sha256(password));
        return await request.post("/authentication/login", {
            email: email,
            password: hashed
        });
    }

    static async logout() {
        return await request.get("/authentication/logout");
    }

    static async forgot_password(email: string) {
        return await request.get("/authentication/forgot-password", {
            params: {
                email: email
            }
        });
    }

    static async second_factor_totp(code: string) {
        return await request.get("/authentication/2fa/totp", {
            params: {
                code: code
            }
        });
    }

    static async list_tokens() {
        return await request.get("/authentication/tokens");
    }

    static async delete_token(token: string="", all: boolean=false) {
        if (all) return await request.delete("/authentication/tokens", { params: { id: "all" } } );
        else return await request.delete("/authentication/tokens", {
            params: {
                id: token
            }
        });
    }
}
