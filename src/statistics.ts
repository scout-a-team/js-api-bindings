import request from "./request";

export class Statistics {
    static async team(number: number) {
        return await request.get(`/statistics/events`, {
            params: {
                team: number
            }
        });
    }

    static async per_match(event: string, team: number) {
        return await request.get(`/statistics/events/${event}/matches`, {
            params: {
                team: team
            }
        });
    }

    static async trueskill_rankings(global: boolean) {
        return await request.get(`/statistics/rankings/skill`, {
            params: {
                global: (global) ? "yes" : "no"
            }
        });
    }

    static async points_rankings(event: string) {
        return await request.get(`/statistics/rankings/points`, {
            params: {
                event: event
            }
        });
    }
}
