import request from "./request";

export class Events {
    static async list() {
        return await request.get(`/events`);
    }

    static async describe(key: string) {
        return await request.get(`/events/${key}`);
    }

    static async matches(key: string) {
        return await request.get(`/events/${key}/matches`);
    }

    static async teams(key: string) {
        return await request.get(`/events/${key}/teams`);
    }

    static async match(event: string, key: string) {
        return await request.get(`/events/${event}/matches/${key}`);
    }
}
