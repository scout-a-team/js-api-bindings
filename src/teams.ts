import request from "./request";

export class Teams {
    static async search(number?: number, name?: string) {
        return await request.get("/teams", {
            params: {
                number: number,
                name: name
            }
        });
    }

    static async describe(id: string) {
        return await request.get(`/teams/${id}`);
    }

    static async update(id: string, data: { name?: string, city?: string, country?: string }) {
        return await request.put(`/teams/${id}`, data);
    }

    static async deleteAccount(id: string) {
        return await request.delete(`/teams/${id}`);
    }

    static async average_event_performance(id: string) {
        return await request.get(`/teams/${id}/stats`);
    }

    static async verified(id: string) {
        return await request.get(`/teams/${id}/verified`);
    }

    static async events(id: string) {
        return await request.get(`/teams/${id}/events`);
    }

    static async matches(id: string) {
        return await request.get(`/teams/${id}/matches`);
    }
}
