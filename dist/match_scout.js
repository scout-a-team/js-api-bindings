"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MatchScout = exports.MalfunctionType = exports.CollectionLocation = exports.ScoreLocation = exports.EventType = void 0;
var request_1 = __importDefault(require("./request"));
var EventType;
(function (EventType) {
    EventType[EventType["ShotTaken"] = 0] = "ShotTaken";
    EventType[EventType["PowerCellCollected"] = 1] = "PowerCellCollected";
    EventType[EventType["ControlPanelStage2"] = 2] = "ControlPanelStage2";
    EventType[EventType["ControlPanelStage3"] = 3] = "ControlPanelStage3";
    EventType[EventType["ShotBlocked"] = 4] = "ShotBlocked";
    EventType[EventType["Malfunction"] = 5] = "Malfunction";
    EventType[EventType["PushMatch"] = 6] = "PushMatch";
    EventType[EventType["Climbing"] = 7] = "Climbing";
})(EventType = exports.EventType || (exports.EventType = {}));
var ScoreLocation;
(function (ScoreLocation) {
    ScoreLocation[ScoreLocation["LowGoal"] = 0] = "LowGoal";
    ScoreLocation[ScoreLocation["OuterGoal"] = 1] = "OuterGoal";
    ScoreLocation[ScoreLocation["InnerGoal"] = 2] = "InnerGoal";
})(ScoreLocation = exports.ScoreLocation || (exports.ScoreLocation = {}));
var CollectionLocation;
(function (CollectionLocation) {
    CollectionLocation[CollectionLocation["FriendlyLoadingStation"] = 0] = "FriendlyLoadingStation";
    CollectionLocation[CollectionLocation["OpponentLoadingStation"] = 1] = "OpponentLoadingStation";
    CollectionLocation[CollectionLocation["FriendlyTrench"] = 2] = "FriendlyTrench";
    CollectionLocation[CollectionLocation["OpponentTrench"] = 3] = "OpponentTrench";
    CollectionLocation[CollectionLocation["FriendlyBoundaries"] = 4] = "FriendlyBoundaries";
    CollectionLocation[CollectionLocation["OpponentBoundaries"] = 5] = "OpponentBoundaries";
    CollectionLocation[CollectionLocation["FriendlySector"] = 6] = "FriendlySector";
    CollectionLocation[CollectionLocation["OpponentSector"] = 7] = "OpponentSector";
    CollectionLocation[CollectionLocation["Other"] = 8] = "Other";
})(CollectionLocation = exports.CollectionLocation || (exports.CollectionLocation = {}));
var MalfunctionType;
(function (MalfunctionType) {
    MalfunctionType[MalfunctionType["Break"] = 0] = "Break";
    MalfunctionType[MalfunctionType["Brownout"] = 1] = "Brownout";
    MalfunctionType[MalfunctionType["Disabled"] = 2] = "Disabled";
    MalfunctionType[MalfunctionType["Other"] = 3] = "Other";
})(MalfunctionType = exports.MalfunctionType || (exports.MalfunctionType = {}));
var MatchScout = /** @class */ (function () {
    function MatchScout() {
    }
    MatchScout.matches = function (key) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, request_1.default.get("/scouting/match/" + key)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    MatchScout.scout = function (key, match, team, events) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, request_1.default.post("/scouting/match/" + key, {
                            match: match,
                            team: team,
                            events: events
                        })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    MatchScout.statistics = function (key) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, request_1.default.get("/scouting/match/" + key + "/stats")];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    return MatchScout;
}());
exports.MatchScout = MatchScout;
