export declare enum EventType {
    ShotTaken = 0,
    PowerCellCollected = 1,
    ControlPanelStage2 = 2,
    ControlPanelStage3 = 3,
    ShotBlocked = 4,
    Malfunction = 5,
    PushMatch = 6,
    Climbing = 7
}
export declare enum ScoreLocation {
    LowGoal = 0,
    OuterGoal = 1,
    InnerGoal = 2
}
export declare enum CollectionLocation {
    FriendlyLoadingStation = 0,
    OpponentLoadingStation = 1,
    FriendlyTrench = 2,
    OpponentTrench = 3,
    FriendlyBoundaries = 4,
    OpponentBoundaries = 5,
    FriendlySector = 6,
    OpponentSector = 7,
    Other = 8
}
export declare enum MalfunctionType {
    Break = 0,
    Brownout = 1,
    Disabled = 2,
    Other = 3
}
export interface MatchEvent {
    type: EventType;
    shots_made?: number;
    shots_taken?: number;
    shot_score_location?: ScoreLocation;
    shot_from?: number;
    collected_success?: boolean;
    collected_from?: CollectionLocation;
    cp_start?: number;
    cp_end?: number;
    cp_attempts?: number;
    cp_completed?: boolean;
    block_successful?: number;
    block_attempted?: number;
    malfunction_start?: number;
    malfunction_end?: number;
    malfunction_type?: MalfunctionType;
    push_start?: number;
    push_end?: number;
    push_against?: number;
    push_won?: boolean;
    climb_start?: number;
    climb_end?: number;
    climb_attempts?: number;
    climb_successful?: boolean;
}
export declare class MatchScout {
    static matches(key: string): Promise<import("axios").AxiosResponse<any>>;
    static scout(key: string, match: string, team: string, events: MatchEvent[]): Promise<import("axios").AxiosResponse<any>>;
    static statistics(key: string): Promise<import("axios").AxiosResponse<any>>;
}
