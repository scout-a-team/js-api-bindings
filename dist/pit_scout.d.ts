export interface PitScoutData {
    weight?: number;
    width?: number;
    length?: number;
    drive_train_motor_count?: number;
    drive_train_motor_type?: "Mini CIM" | "CIM" | "Neo" | "Falcon";
    drive_train_type?: "Tank" | "Swerve" | "Mecanum" | "Tank Treads";
    wheel_type?: "Traction" | "Colson" | "Pneumatic" | "Omni";
    wheel_diameter?: 4 | 6 | 8;
    programming_language?: "Java" | "C++" | "LabView";
    camera?: boolean;
    notes?: string;
    game_specific: PitScoutGameSpecificData;
}
export interface PitScoutGameSpecificData {
    climbing_method?: string;
    move_on_bar?: boolean;
    carry_robot?: boolean;
    low_goal?: boolean;
    outer_goal?: boolean;
    inner_goal?: boolean;
}
export declare class PitScout {
    static list(): Promise<import("axios").AxiosResponse<any>>;
    static team(key: string): Promise<import("axios").AxiosResponse<any>>;
    static modify(key: string, data: PitScoutData): Promise<import("axios").AxiosResponse<any>>;
}
