declare const _default: import("axios").AxiosInstance;
export default _default;
export declare class Requester {
    static get(path: string, params?: Object): Promise<{
        status: number;
        success: boolean;
        reason: any;
        data: any;
    } | {
        status: number;
        success: boolean;
        reason: any;
    }>;
    static post(path: string, body?: Object): Promise<void>;
    static put(path: string, body?: Object): Promise<void>;
    static delete(path: string, params?: Object): Promise<void>;
}
