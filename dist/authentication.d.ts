export declare class Authentication {
    static register(first_name: string, last_name: string, email: string, password: string, team_number: number): Promise<import("axios").AxiosResponse<any>>;
    static login(email: string, password: string): Promise<import("axios").AxiosResponse<any>>;
    static logout(): Promise<import("axios").AxiosResponse<any>>;
    static forgot_password(email: string): Promise<import("axios").AxiosResponse<any>>;
    static second_factor_totp(code: string): Promise<import("axios").AxiosResponse<any>>;
    static list_tokens(): Promise<import("axios").AxiosResponse<any>>;
    static delete_token(token?: string, all?: boolean): Promise<import("axios").AxiosResponse<any>>;
}
