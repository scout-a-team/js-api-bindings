export declare class Teams {
    static search(number?: number, name?: string): Promise<import("axios").AxiosResponse<any>>;
    static describe(id: string): Promise<import("axios").AxiosResponse<any>>;
    static update(id: string, data: {
        name?: string;
        city?: string;
        country?: string;
    }): Promise<import("axios").AxiosResponse<any>>;
    static deleteAccount(id: string): Promise<import("axios").AxiosResponse<any>>;
    static average_event_performance(id: string): Promise<import("axios").AxiosResponse<any>>;
    static verified(id: string): Promise<import("axios").AxiosResponse<any>>;
    static events(id: string): Promise<import("axios").AxiosResponse<any>>;
    static matches(id: string): Promise<import("axios").AxiosResponse<any>>;
}
