export declare class Statistics {
    static team(number: number): Promise<import("axios").AxiosResponse<any>>;
    static per_match(event: string, team: number): Promise<import("axios").AxiosResponse<any>>;
    static trueskill_rankings(global: boolean): Promise<import("axios").AxiosResponse<any>>;
    static points_rankings(event: string): Promise<import("axios").AxiosResponse<any>>;
}
