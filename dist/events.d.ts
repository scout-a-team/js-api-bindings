export declare class Events {
    static list(): Promise<import("axios").AxiosResponse<any>>;
    static describe(key: string): Promise<import("axios").AxiosResponse<any>>;
    static matches(key: string): Promise<import("axios").AxiosResponse<any>>;
    static teams(key: string): Promise<import("axios").AxiosResponse<any>>;
    static match(event: string, key: string): Promise<import("axios").AxiosResponse<any>>;
}
