export declare enum TeamVerificationMethod {
    WEB_PAGE = 1,
    DNS = 2,
    PHOTO = 3
}
export declare class Verification {
    static email_verification(token: string): Promise<import("axios").AxiosResponse<any>>;
    static get_team_verification(): Promise<import("axios").AxiosResponse<any>>;
    static verify_team(type: TeamVerificationMethod, page?: string, domain?: string, image?: string): Promise<import("axios").AxiosResponse<any>>;
    static reset_password(password: string): Promise<import("axios").AxiosResponse<any>>;
}
