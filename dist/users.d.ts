export declare type Role = "owner" | "administrator" | "mentor" | "member" | "";
export declare class Users {
    static list(team: string): Promise<import("axios").AxiosResponse<any>>;
    static create(team: string, first_name: string, last_name: string, email: string, password: string, role: string): Promise<import("axios").AxiosResponse<any>>;
    static describe(team: string, id: string): Promise<import("axios").AxiosResponse<any>>;
    static update(team: string, id: string, data: {
        first_name?: string;
        last_name?: string;
        email?: string;
        role?: string;
        enabled?: boolean;
    }): Promise<import("axios").AxiosResponse<any>>;
    static delete(team: string, id: string): Promise<import("axios").AxiosResponse<any>>;
    static enable_2fa(team: string, id: string, totp?: boolean, webauthn?: boolean): Promise<import("axios").AxiosResponse<any>>;
    static disable_2fa(team: string, id: string, method: string): Promise<import("axios").AxiosResponse<any>>;
    static assign(team: string, id: string, role: Role): Promise<import("axios").AxiosResponse<any>>;
}
